// Counting <li> elements in ul.List, changing properly header <h1>:
function taskCounter() {
  var listLength = document.querySelector('ul.List').childElementCount;
  document.getElementById('count_elems').innerHTML=listLength;
  console.log('Tasks: ' + listLength);
  if (listLength == 0) {
    document.getElementById('count_elems').innerHTML='no';
    document.getElementById('tsk').innerHTML=" tasks currently. You can add below your first task!";
  }
  if (listLength == 1) {
    document.getElementById('tsk').innerHTML=" task:";
  }
  if (listLength > 1) {
    document.getElementById('tsk').innerHTML=" tasks:";
  }
}

// Removing tasks on clicking from ul.List
var tasks = document.querySelectorAll('label.elem');
for (var i = 0; i < tasks.length; i++) {
  var task = tasks[i];
  task.onclick = taskRemoving;
}

function taskRemoving(ev) {
  console.log();
  console.log('Target\'s tag: ' + ev.target.tagName);
  if (ev.target.tagName === 'LABEL') {
    ev.target.parentNode.remove();
  }
  else {
  ev.target.parentNode.parentNode.remove();
  }
  taskCounter();
}

// Adding task from form (input[type="text"]) on submitting or clicking plus button
var greenPlus = document.getElementsByClassName("fa fa-plus-square-o")[0];
greenPlus.onclick = taskAdding;

function taskAdding() {
  var task_txt = document.getElementById('form').value;
  if (task_txt == "") {
    event.preventDefault()
  }
  else {
    var ul = document.querySelector('ul.List');
    var new_task = document.createElement('li');
    var label = document.createElement('label');
    var div = document.createElement('div');
    var span = document.createElement('span');
    console.log("Form: " + task_txt);
    label.className = 'elem';
    div.className = 'box fa fa-check';
    span.className = 'text';
    ul.appendChild(new_task);
    new_task.appendChild(label);
    label.appendChild(div);
    label.appendChild(span);
    span.appendChild(document.createTextNode(" " + task_txt));
    document.getElementsByClassName('add_task')[0].reset();
    event.preventDefault();
    label.onclick = taskRemoving;
    taskCounter();
  }
}
